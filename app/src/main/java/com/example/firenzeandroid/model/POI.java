package com.example.firenzeandroid.model;

import androidx.annotation.NonNull;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class POI implements Serializable {
    private String nome;
    private String indirizzo;
    private String telefono;
    private String email;
    private String sitointernet;
    private Double latitudine;
    private Double longitudine;
    private String immagineprincipale;
    private Double rating;

    @NonNull
    @Override
    public String toString() {
        return nome;
    }
}
