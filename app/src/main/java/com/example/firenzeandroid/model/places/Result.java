
package com.example.firenzeandroid.model.places;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Result {


    @SerializedName("business_status")
    private String businessStatus;

    @SerializedName("formatted_address")
    private String formattedAddress;

    private Geometry geometry;
    private String icon;
    private String name;

    @SerializedName("opening_hours")
    private OpeningHours openingHours;

    private List<Photo> photos = null;

    @SerializedName("place_id")
    private String placeId;
    private Double rating;
    private String reference;
    private List<String> types = null;

    @SerializedName("user_ratings_total")
    private Integer userRatingsTotal;
    private Boolean permanentlyClosed;

}
