package com.example.firenzeandroid.model;

import java.util.LinkedList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Categoria {

    private String id;
    private String nome;

    @Override
    public String toString() {
        return nome;
    }

    public static List<Categoria> Categorie = new LinkedList<>(List.of(
            new Categoria("1", "Ristoranti"),
            new Categoria("2", "Alberghi"),
            new Categoria("3", "Negozi"),
            new Categoria("4", "Servizi"),
            new Categoria("5", "Sport"),
            new Categoria("6", "Uffici"),
            new Categoria("7", "Monumenti"),
            new Categoria("8", "Musei")));


}

