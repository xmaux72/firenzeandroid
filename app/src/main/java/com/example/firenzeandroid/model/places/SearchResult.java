
package com.example.firenzeandroid.model.places;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SearchResult {

    @SerializedName("next_page_token")
    private String nextPageToken;
    private List<Result> results = null;
    private String status;

}
