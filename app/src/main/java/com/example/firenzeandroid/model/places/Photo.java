
package com.example.firenzeandroid.model.places;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Photo {

    private Integer height;


    @SerializedName("photo_reference")
    private String photoReference;
    private Integer width;

}
