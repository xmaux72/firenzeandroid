package com.example.firenzeandroid.adapter;

import android.content.ClipData;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.firenzeandroid.R;
import com.example.firenzeandroid.model.POI;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PoiListAdapter extends ArrayAdapter<POI> {

    private int resourceLayout;
    private Context mContext;

    public PoiListAdapter(@NonNull Context context, int resource, @NonNull List<POI> objects) {
        super(context, resource, objects);
        resourceLayout = resource;
        mContext = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(mContext);
            v = vi.inflate(resourceLayout, null);
        }

        POI p = getItem(position);

        if (p != null) {
            TextView textViewPoi = (TextView) v.findViewById(R.id.textViewPoi);
            textViewPoi.setText(p.getNome());

            if(p.getImmagineprincipale() != null) {
                ImageView imageView = (ImageView) v.findViewById(R.id.poiIcon);
                Picasso.get().load(p.getImmagineprincipale()).into(imageView);
            }

            TextView textViewRating = (TextView) v.findViewById(R.id.textViewRating);
            textViewRating.setText(String.valueOf(p.getRating()));


        }

        return v;
    }
}
