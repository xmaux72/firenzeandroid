package com.example.firenzeandroid.rest;

import com.example.firenzeandroid.model.places.SearchResult;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GooglePlacesResource {

    @GET("place/textsearch/json")
    Call<SearchResult> GetSearchResult(
            @Query("key") String key,
            @Query("inputtype") String inputType,
            @Query("input") String input);


}
