package com.example.firenzeandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.firenzeandroid.model.Categoria;
import com.example.firenzeandroid.model.POI;
import com.example.firenzeandroid.model.places.SearchResult;
import com.example.firenzeandroid.rest.GooglePlacesResource;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    ListView categorieListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        prepareListView();
    }

    private void prepareListView() {
        Context localContext = this;
        categorieListView = (ListView)findViewById(R.id.categorieListView);

        final ArrayAdapter<Categoria> adapter = new ArrayAdapter<Categoria>(this,
                R.layout.category_item, R.id.textViewCategoria, Categoria.Categorie);

        categorieListView.setAdapter(adapter);
        categorieListView.setOnItemClickListener((parent, view, position, id) -> {
            Categoria categoria = Categoria.Categorie.get(position);

            Toast.makeText(getApplicationContext(), "Hai selezionato " + categoria, Toast.LENGTH_SHORT).show();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://maps.googleapis.com/maps/api/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            GooglePlacesResource service = retrofit.create(GooglePlacesResource.class);
            Call<SearchResult> call = service.GetSearchResult(
                    getString(R.string.api_key),
                    getString(R.string.inputType),
                    "firenze+" + categoria.getNome());


            call.enqueue(new Callback<SearchResult>() {
                @Override
                public void onResponse(Call<SearchResult> call, Response<SearchResult> response) {
                    SearchResult result=response.body();

                    List<POI> pois = result.getResults().stream()
                            .map(r -> new POI(
                                    r.getName(),
                                    r.getFormattedAddress(),
                                    "055/54123145",
                                    "N.A.",
                                    "N.A.",
                                    r.getGeometry().location.lat,
                                    r.getGeometry().location.lng,
                                    r.getIcon(),
                                    r.getRating()))
                            .sorted(Comparator.comparingDouble(POI::getRating).reversed())
                            .collect(Collectors.toList());

                    Intent intent = new Intent(localContext, PoiListActivity.class);
                    intent.putExtra("pois", new ArrayList<POI>(pois));
                    intent.putExtra("title", categoria.getNome());
                    startActivity(intent);
                }

                @Override
                public void onFailure(Call<SearchResult> call, Throwable t) {
                    //Handle failure
                }
            });


        });

    }


}