package com.example.firenzeandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.firenzeandroid.adapter.PoiListAdapter;
import com.example.firenzeandroid.model.Categoria;
import com.example.firenzeandroid.model.POI;
import com.example.firenzeandroid.model.places.SearchResult;
import com.example.firenzeandroid.rest.GooglePlacesResource;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PoiListActivity extends AppCompatActivity {

    List<POI> pois;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poi_list);

        pois = (List<POI>) getIntent()
                .getSerializableExtra("pois");

        ((TextView) findViewById(R.id.mainTitle)).setText(getIntent().getStringExtra("title"));

        prepareListView();


    }

    private void prepareListView() {
        Context localContext = this;
        ListView poiListView = (ListView) findViewById(R.id.poiListView);

        final PoiListAdapter adapter = new PoiListAdapter(this,
                R.layout.poi_item, pois);

        poiListView.setAdapter(adapter);
        poiListView.setOnItemClickListener((parent, view, position, id) -> {
            POI poi = pois.get(position);

            Toast.makeText(getApplicationContext(), "Hai selezionato " + poi, Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(localContext, PoiDetailActivity.class);
            intent.putExtra("poi", poi);
            startActivity(intent);
        });

    }
}