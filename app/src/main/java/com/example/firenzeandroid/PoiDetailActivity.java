package com.example.firenzeandroid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.firenzeandroid.model.POI;

import java.util.List;

public class PoiDetailActivity extends AppCompatActivity {
    
    POI poi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poi_detail);

        poi = (POI) getIntent().getSerializableExtra("poi");

        prepareView();
    }

    private void prepareView() {

        MapsFragment mapsFragment = (MapsFragment) getSupportFragmentManager().findFragmentById(R.id.mapsFragment);
        mapsFragment.setInfo(poi);

        ((TextView) findViewById(R.id.mainTitle)).setText(poi.getNome());
        ((TextView) findViewById(R.id.textViewIndirizzo)).setText(poi.getIndirizzo());
        ((TextView) findViewById(R.id.textViewTelefono)).setText(Html.fromHtml( "<b>Tel:</b> " + poi.getTelefono()));
        ((TextView) findViewById(R.id.textViewEmail)).setText(Html.fromHtml("<b>Email:</b> " + poi.getEmail()));
        ((TextView) findViewById(R.id.textViewWebsite)).setText(Html.fromHtml("<b>Website:</b> " + poi.getSitointernet()));
        ((ImageButton) findViewById(R.id.imageButtonPhone)).setOnClickListener(v -> {
            if (ContextCompat.checkSelfPermission(PoiDetailActivity.this,  Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED)
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + poi.getTelefono())));


        });




    }
}